﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {
	public float speed;
	public float hop;
	public Vector3 jump;
	public Text countText;
	public Text winText;
	private Rigidbody rb;
	private int count;
	private bool onGround;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody>();
		count = 0;
		SetCountText();
		winText.text = "";
		onGround = true;
		jump = new Vector3(0.0f, 2.0f, 0.0f);
	}
	
	// Update is called once per fram

	void FixedUpdate() {
		float moveHorizontal = Input.GetAxis("Horizontal");
		float moveVertical = Input.GetAxis("Vertical");
		if (Input.GetKeyDown(KeyCode.Space))
		{
			if (onGround == true)
			{
				rb.AddForce(hop * jump, ForceMode.Impulse);
				onGround = false;
			}
		}

		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
		rb.AddForce (movement * speed);

		if (Input.GetKeyDown(KeyCode.Escape))
		{
			SceneManager.LoadScene(0);
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag("Pick Up"))
		{
			other.gameObject.SetActive (false);
			count += 1;
			SetCountText();
		}
	}

	void OnCollisionEnter (Collision col)
	{
		if(col.gameObject.CompareTag("Ground"))
		{
			onGround = true;
		}
	}

	void SetCountText()
	{
		countText.text = "Count: " + count.ToString();
		if (count >= 12)
		{
			winText.text = "You Win!";
		}
	}
}
